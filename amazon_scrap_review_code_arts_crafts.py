from bs4 import BeautifulSoup as bs
import requests as rs
from fake_useragent import UserAgent
import html5lib
import pandas as pd
import csv
import time

from requests.exceptions import InvalidSchema
from requests.exceptions import MissingSchema

ua = UserAgent()
header = {'User-Agent':str(ua.chrome)}

'''retriving the csv '''
rows=[]

with open('amazon_artscraftssewing_url.csv', 'rb') as f:
	reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
	for row in reader:
		rows.append(row)

artscraftsewing = []
ua = UserAgent()
header = {'User-Agent':str(ua.chrome)}

with open('amazon_pandora_arts_crafts.csv', 'a+') as myfile:
	wr = csv.writer(myfile, delimiter =",")
	for one_row in range(len(rows[0])):
		url= rows[0][one_row]
		artscraftsewing = []

		xasin = ""
		xprod_name = ""
		xsubclass = ""
		xreview_stars = ""
		xdate = ""
		xhealpful = ""
		xheader = ""
		xreview_body = ""

		try:
			url_parsed = rs.get(url,  headers = header) #1
			review_parsed = bs(url_parsed.text, 'html5lib')

			try:
				'''1. REVIEW STARS'''
				reviews_stars = review_parsed.find_all("span", class_="a-icon-alt")
				xreview_stars = reviews_stars[0].text  # avg star of the  reviews
			except IndexError:
				pass

			'''2. ASIN'''
			for tag in review_parsed.find_all("div",{'id':"cerberus-data-metrics"}) :
				xasin = (tag['data-asin'])

			'''3. product name'''
			try:
				name = review_parsed.find_all("span", class_="a-size-large")
				xprod_name = name[0].text.strip()
			except IndexError:
				pass
			'''5. goto specific subclass'''
			try:
				subclass = review_parsed.find_all("a", class_="a-link-normal a-color-tertiary")
				t = len(subclass)
				xsubclass = subclass[t-1].text.strip()
			except IndexError:
				pass

			try:
				'''. customer review url and parsing'''
				t = review_parsed.find_all("a", class_="a-link-emphasis a-text-bold")
				urll = "https://www.amazon.com/"+str(t[0].get("href"))
				prod_review = rs.get(urll, headers = header) #1
				soup_prod = bs(prod_review.text, 'html.parser' )

				'''remember customer review section starts here'''

				'''6. review_header'''
				try:
					header_reviews = soup_prod.find_all("a", class_="a-size-base a-link-normal review-title a-color-base a-text-bold")
				except IndexError:
					pass

				'''7. review helpfull ness'''
				try:
					helpful = soup_prod.find_all("span", class_="review-votes")
					xhealpful = helpful[0].text
				except IndexError:
					pass

				try:
					'''8. review date'''
					header_reviews = soup_prod.find_all("span", class_="a-size-base a-color-secondary review-date")
					xdate = header_reviews[0].text
				except IndexError:
					pass

				'''9. review_body''' #be careful about iterable
				try:
					total_reviews = soup_prod.find_all("span", class_="a-size-base review-text")
					for t in range(len(total_reviews)):
						xreview_body = total_reviews[t].text
						xheader = header_reviews[t].text

						artscraftsewing_x = [xasin, xprod_name,  xsubclass, xreview_stars, xdate,
												   xhealpful, xheader, xreview_body]

						wr.writerow([
							xasin.encode('utf-8'),
							xprod_name.encode('utf-8'),
							xsubclass.encode('utf-8'),
							xreview_stars.encode('utf-8'),
							xdate.encode('utf-8'),
							xhealpful.encode('utf-8'),
							xheader.encode('utf-8'),
							xreview_body.encode('utf-8')
						])

				except IndexError:
					print("Sleeping1 for 20 secs.")
					time.sleep(20)
					pass
			except (MissingSchema, InvalidSchema, IndexError):
				print("Sleeping1 for 20 secs.")
				time.sleep(20)
				pass
		except (InvalidSchema, MissingSchema):
			pass
