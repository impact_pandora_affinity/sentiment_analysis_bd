# -*- coding: utf-8 -*-
"""
Created on Thu May 17 17:52:17 2018

@author: harshitha
"""
import os
import pandas as pd
import re
import string
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
from nltk.corpus import stopwords
set(stopwords.words('english'))


def sentiment_polarity_tag(Dataset_rev,source):
    Dataset_rev.rename(columns={'date':'Date','reviews':'Review'}, inplace = True)
    Dataset_comp = Dataset_rev[pd.notna(Dataset_rev.Review)]
    Dataset_comp = Dataset_comp[Dataset_rev.Review != " "]
    sid = SentimentIntensityAnalyzer()
    Dataset_comp.reset_index(inplace = True)
    Dataset_comp.drop(['index'] ,axis = 1, inplace = True)
    
    
    def Add_full_stop(sentence):
        if sentence.strip()[-1] == '.':
            return sentence+'.'
        else:
            return sentence.strip()+'.'
        
    
    
    Dataset_comp["Review_wPunct"] = Dataset_comp["Review"].apply(Add_full_stop)
            
    
    splitter = lambda x: re.split('(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', x)
    trimmed_splitter = lambda sentence: [part.strip() for part in splitter(sentence)]
    
    cust_review = Dataset_comp[['Review_wPunct','Date']]

    
    review_sentences ,Dates = [], []        
    for i  in range(len(cust_review)):
        sentences = trimmed_splitter(cust_review.Review_wPunct[i])
        sentences = [sent for sent in sentences if sent]
        review_sentences += sentences
        Dates += [str(cust_review.Date[i])]* len(sentences)   
        
    final_review = pd.DataFrame(
        {
         'review_sentences': review_sentences,
         'Date':Dates
         }
    )
    
    final_review = final_review.drop_duplicates(keep='first')
        
    def strip_links(text):
        link_regex    = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
        links         = re.findall(link_regex, text)
        for link in links:
            text = text.replace(link[0], ', ')    
        return text
    
    def strip_all_entities(text):
        entity_prefixes = ['@','#']
        for separator in  string.punctuation:
            if separator not in entity_prefixes :
                text = text.replace(separator,' ')
        words = []
        for word in text.split():
            word = word.strip()
            if word:
                if word[0] not in entity_prefixes:
                    words.append(word)
        return ' '.join(words)
    
    final_review["review_sentences"] = final_review["review_sentences"].apply(strip_links)
    final_review["review_sentences"] = final_review["review_sentences"].apply(strip_all_entities)
    final_review = final_review[final_review.review_sentences != ""]
    final_review['source']=source
    final_review["Date"] = pd.to_datetime(final_review["Date"])
    
    def sentimenter(sentence):
        sentiment_score = sid.polarity_scores(sentence)
        score = sentiment_score['compound']
        return score
    
    def sentiment_tag(score):
        if score >= 0.25:
            return 'positive'
        elif score <= -0.25:
            return 'negative'
        else:
            return 'neutral'
    
    
    final_review["Score"] = final_review['review_sentences'].apply(sentimenter)
    final_review["Sentiment"] = final_review['Score'].apply(sentiment_tag)
    final_review.reset_index(inplace = True)
    final_review.drop(['index'] ,axis = 1, inplace = True)
    final_review["ID"] = final_review["source"]+"_"+final_review["Date"].map(str)+"_"+final_review.index.map(str)
   
iew)


def review_cleanser(tagged_review):
    stop_wordss = ['pandora','jewellery','bought','jewelry','like','would','said','told','want','could','never'
                   'given','give','asked','took','well','able','also','today','took','another','nothing','john','joey']+list(string.ascii_lowercase)
    
    rev_string = ''.join(map(str, tagged_review))
    rev_string2 = re.sub("[^a-zA-Z]"," ",str(rev_string))
    sentence = rev_string2.strip()
    sentence = sentence.split()
    
    new_lemma = []
    for i in range(len(sentence)):
        new_lemma.append((lemmatizer.lemmatize(sentence[i])).encode('utf-8'))
    
    new_lemma = map(lambda x:x.lower(),new_lemma)  
    gSW = stopwords.words("english")
    
    for words in stop_wordss:
        gSW.append(words)
    
    cloud_words = []
    for clean_words in new_lemma:
        if clean_words not in gSW:
            if len(clean_words)>3:
                cloud_words.append(clean_words)
    cleaned_rev = ' '.join(map(str,cloud_words))
    return(cleaned_rev)
    
    
# Change path, keep only 1 file in the folder    
# Keep the input file in the old cleaned file format
os.chdir("E:\Projects\Social_media_SA\datasets\Alex_Ani")

mypath = "E:\Projects\Social_media_SA\datasets\Alex_Ani"
from os import listdir 
from os.path import isfile ,join
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]

All_data =pd.DataFrame()
for fil in files:
    Dataset_rev = pd.read_csv(fil)
    source = Dataset_rev.source.unique()[0]
    print source
    Sentiment_tagged = sentiment_polarity_tag(Dataset_rev,source)
    Sentiment_tagged["cleaned_review"] = Sentiment_tagged["review_sentences"].apply(review_cleanser)
    All_data = All_data.append(Sentiment_tagged)
# write to a different folder
All_data.to_csv("Cleaned_dt_AA.csv", encoding = 'utf-8', index = False)
