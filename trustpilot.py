from bs4 import BeautifulSoup as bs
import requests as rs
import pandas as pd
from pprint import pprint
import timestring

'''page url'''
t_p='https://www.trustpilot.com/review/www.pandora.net'
page= '?page='

'''using request module'''
r= rs.get(t_p) #1

'''using bs4 to parse'''
soup = bs(r.text, 'html.parser')

'''review and time from soup'''
soup.p.text
soup.time.text

'''code to get all pages review'''
tpp = []

for i in range(5):
    page = rs.get(t_p + '?page=' + str(i))
    r= rs.get(t_p)
    soup = bs(r.text, 'html.parser')

    for review in (soup.find_all('p')):
        tpp.append(review.text)

df = pd.DataFrame({'col':tpp})
df.head()


'''code to get all pages date'''
date = []

for i in range(5):
    page = rs.get(t_p + '?page=' + str(i))
    r= rs.get(t_p)
    soup = bs(r.text, 'html.parser')


    for dates in soup.find_all("time"):
        date.append(dates.text.strip("\n Published"))


df1 = pd.DataFrame({'date':date})
display(df1.head())

'''concat the two df'''
df1.reset_index(drop=True, inplace=True)
df.reset_index(drop=True, inplace=True)
frames = [df, df1]

result = pd.concat(frames,axis=1)
result.head()
