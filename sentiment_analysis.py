
# coding: utf-8

# # currently running type tag

# In[1]:


import pandas as pd
import os
from datetime import datetime, timedelta
# from Subject_identify_Jewelry import get_max_score, get_tags, dedupe_str
import time
import en_core_web_md
import re


# In[2]:


nlp = en_core_web_md.load()


# tag_dict = {'theme'	:	['3 Dimensional', 'Abstract ', 'Angel ', 'Antiqued ', 'Ball Chain ', 'Baptism', 'Baroque Cross ', 'Bead', 'Brushed ', 'Budded Cross', 'Bullet ', 'Cat ', 'Chai ', 'Chandelier ', 'Charm Holder', 'Circle', 'Circular', 'Coin Holder ', 'Concave ', 'Confirmation ', 'Cord ', 'Crown ', 'Crown Heart ', 'Cylinder', 'Dangle ', 'Daughter ', 'Desire Heart ', 'Diamond Shape ', 'Dove ', 'Drop ', 'Ecce Homo', 'Eternal Life ', 'Eye ', 'Face of Jesus ', 'Filigree', 'Freeform ', 'Gold Bar ', 'Graduated Squares ', 'Grandma ', 'Greek Cross ', 'Greek Key ', 'H Key ', 'Hammered ', 'Heavy Weight ', 'Holy Spirit', 'Infant of Prague ', 'Kings Crown ', 'Latin Cross ', 'Link ', 'Love You Always ', 'Matka Boska ', 'Matka Boska Medal ', 'Mezuzah ', 'Miraculous ', 'Mom ', 'Moon ', 'Moveable ', 'Music Note ', 'Number 1 ', 'Number Top ', 'Omega ', 'Orange ', 'Orthodox Cross ', 'Our Lady of Fatima ', 'Our Lady of Lourdes ', 'Our Lady of Mount Carmel ', 'Our Lady of Perpetual Help ', 'Oval ', 'Passion Cross ', 'Patonce Cross ', 'Patterned ', 'Photo ', 'Puffed ', 'Puffed Heart ', 'Radiant Essence ', 'Rectangle ', 'Rectangular ', 'Rectangular Shaped ', 'Recycle ', 'Roller Skate ', 'Rope ', 'Rope Cross ', 'Round ', 'Sacred Heart of Jesus ', 'Sacred Heart of Mary ', 'Scalloped Disc ', 'Scapular ', 'Scroll ', 'Shield ', 'Special Sister ', 'Square ', 'Square Swirl ', 'Square with Circles ', 'St. Elizabeth ', 'St. Gerard ', 'St. James ', 'St. Jude Thaddeus ', 'St. Martha ', 'St. Padre Pio ', 'St. Roch ', 'Strength ', 'Sun Cross ', 'Sunburst ', 'Swirl ', 'Swirl Cross ', 'Swirl Design ', 'Swirled Heart ', 'Tear Drop ', 'Teardrop ', 'Textured ', 'Top ', 'Triangular ', 'Twisted ', 'Twisted Rope ', 'US Coast Guard ', 'Wavy Circle ', 'Whale Tail ', 'flower']	,
# 'charms'	:	['Animals', 'Awareness Ribbons', 'Branded', 'Firefighter', 'Flowers', 'Trees', 'Plants', 'Food ', 'Drink', 'Fraternal', 'Hearts ', 'Horoscope', 'Irish', 'Celtic', 'Claddagh', 'Letters', 'Numbers', 'Names', 'Life Events', 'Locations ', 'Places', 'Medical', 'Military', 'Music', 'Nautical', 'Patriotic', 'People ', 'Family', 'Planes', 'Trains', 'Autos', 'Police', 'Religious', 'Shoes ', 'Clothing', 'Skulls', 'Sororities', 'Sports ', ' Games', 'Sports Teams', 'Sun', 'Moon', 'Stars', 'Talking', 'Tools', 'Trade', 'Wedding Gifts']	,
# 'characteristics'	:	['sparkle', 'shining', 'luminous', 'light', 'weight', 'heavy', 'versatile', 'sturdy', 'glint']	,
# 'price'	:	['affordable', 'cheap']	,
# 'material'	:	['.925 sterling silver', '10k gold', 'Swarovski crystal', 'acrylic', 'amber', 'amethyst', 'aquamarine', 'birthstone', 'black pearl', 'carnelian', 'ceramic', 'citrine', 'copper', 'mother of pearl', 'obsidia', 'onyx', 'pearl', 'peridot', 'platinum', 'precious gem', 'resin', 'rhinestone', 'rhodium', 'ruby', 'sapphire', 'seed beads', 'crystal', 'cubic zirconia', 'diamond', 'emerald', 'enamel', 'freshwater pearl', 'garnet', 'gem', 'gemstone', 'jade', 'laboratory gemstone', 'lapis', 'lucite', 'semi-precious stones', 'silver', 'simulated gemstone', 'stainless steel', 'titanium', 'topaz', 'tungsten', 'turquoise', 'white gold', 'wood', 'yellow gold', 'bead']	,
# 'discount'	:	['honest opinion']	,
# 'packing'	:	['wrap', 'securely', 'enclosed', 'warranty', 'boxes']	,
# 'negatives'	:	['abrasion', 'break', 'cheap', 'chipped', 'clash', 'digs in', 'poor fit', 'poor quality', 'rigid', 'rub', 'run-of-the-mill', 'scratch', 'scratches easily', 'falls apart', 'flawed', 'full-priced', 'heavy', 'ill-fitting', 'irregularities', 'irritation', 'tight', 'uncomfortable', 'unfashionable', 'unflattering', 'unwearable', 'wear and tear']	,
# 'positives'	:	['loved it', 'love', 'stunning', 'beautiful', 'brilliant', 'shining', 'bling', 'highly recommend', 'pretty', 'elegant', 'classy', 'compliments', 'antique', 'endearing', 'perfect', 'pro']	,
# 'style'	:	['Rings', 'Earrings', 'Pendants', 'Charms', 'Bracelets', 'Necklaces', 'Chains', 'Collections', 'Anklets', 'Watches', 'Closeouts']	,
# 'colour'	:	['Yellow Gold', 'Sterling Silver ', 'Stainless Steel ', 'White Gold ', 'White And Yellow Gold ', 'Gold Filled ', 'Rose And White Gold ', 'Tungsten', 'Titanium', 'Rose Gold', 'Tri Color Gold', 'Rhodium Flashed Silver', 'Yellow Gold Flashed Silver', 'Rhodium Plated Silver']	,
# 'gender and relationship'	:	['male', 'female', 'girl', 'grandmother', 'daughter', 'kid', 'child', 'wife', 'husband', 'sister', 'daughter in law', 'mother in law', 'sister in law', 'girl friend', 'boyfriend', 'son', 'friend']	,
# 'shipping'	:	['fast', 'quick', 'eta', 'late', 'early', 'delivery', 'ahead', 'promt', 'undamaged']	,
# 'adjectives'	:	['adjustable', 'adorable', 'antique', 'artisan', 'artisanal', 'attention-getting', 'bangle-style', 'beaded', 'beautiful', 'bejeweled', 'bold', 'brilliant', 'burnished', 'carved', 'casual', 'certified', 'channel-set', 'charming', 'chic', 'chunky', 'classic', 'clustered', 'colorful', 'comfortable', 'comfy', 'complex', 'contemporary', 'cool', 'coordinating', 'corrosion-resistant', 'costume', 'crackled', 'cut-out', 'cute', 'cutting-edge', 'dainty', 'dangling', 'dangly', 'dapper', 'decorative', 'delicate', 'dependable', 'designer', 'detailed', 'discounted', 'distinctive', 'dramatic', 'durable', 'easy-to-maintain', 'eco-friendly', 'edgy', 'elegant', 'inlaid', 'innovative', 'intricate', 'iridescent', 'jewel-tone', 'keepsake', 'kiln-fired', 'large', 'laser-cut', 'latest', 'lead-free', 'lightweight', 'long-lasting', 'lovely', 'marbled', 'marquise-cut', 'masculine', 'men', 'metallic', 'minimalist', 'mosaic', 'multi-faceted', 'multistrand', 'nickel-free', 'one-of-a-kind', 'opaque', 'open-worked', 'original', 'ornate', 'period', 'pierced', 'plated', 'platinum', 'polished', 'pre-formed', 'precious', 'precise', 'premium-grade', 'preppy', 'princess-cut', 'prismatic', 'professional', 'radiant', 'reflective', 'reliable', 'rocker-style', 'rough-cut', 'round-cut', 'rugged', 'rust-resistant', 'sale-priced', 'sassy', 'encased', 'engineered', 'engraved', 'etched', 'everyday', 'exceptional', 'exciting', 'exotic', 'expensive-looking', 'exquisite', 'eye-catching', 'faceted', 'fancy', 'fashion-forward', 'fashionable', 'favorite', 'feminine', 'fine', 'finely detailed', 'finished with', 'flattering', 'flawless', 'flexible', 'flirty', 'floral', 'funky', 'genuine', 'glamorous', 'glittering', 'glitzy', 'gold', 'gold-filled', 'gold-plated', 'gold-toned', 'gorgeous', 'graceful', 'gunmetal', 'half-priced', 'hammered', 'hand-carved', 'hand-crafted', 'hand-finished', 'hand-hammered', 'hand-wrapped', 'handcrafted', 'handmade', 'hassle-free', 'high-class', 'high-performance', 'hinged', 'hot', 'hypoallergenic', 'scratch-resistant', 'semi-translucent', 'set', 'sexy', 'shimmering', 'silver', 'silver-tone', 'simple', 'sleek', 'slender', 'slip-on', 'small', 'smoky', 'smooth', 'snag-free', 'solid', 'sophisticated', 'sparkling', 'sparkly', 'sporty', 'streamlined', 'striking', 'structural', 'studded', 'stunning', 'stylish', 'subtle', 'superior', 'supportive', 'suspended', 'tapered', 'teardrop', 'textured', 'timeless', 'tiny', 'top-of-the-line', 'trendsetting', 'tribal', 'two-tone', 'unique', 'versatile', 'vintage', 'wardrobe-friendly', 'water-resistant', 'waterproof', 'wear-anywhere', 'whimsical', 'women', 'wooden', 'wrapped']	,
# 'packing bad'	:	['worn', 'broken', 'lost', 'bent', 'spin', 'fall out', 'out', 'loose', 'discolouration', 'damaged']}	



# In[ ]:


# tag_dict =# {'theme'    :    ['3 Dimensional', 'Abstract ', 'Angel ', 'Antiqued ', 'Ball Chain ', 'Baptism', 'Baroque Cross ', 'Bead', 'Brushed ', 'Budded Cross', 'Bullet ', 'Cat ', 'Chai ', 'Chandelier ', 'Charm Holder', 'Circle', 'Circular', 'Coin Holder ', 'Concave ', 'Confirmation ', 'Cord ', 'Crown ', 'Crown Heart ', 'Cylinder', 'Dangle ', 'Daughter ', 'Desire Heart ', 'Diamond Shape ', 'Dove ', 'Drop ', 'Ecce Homo', 'Eternal Life ', 'Eye ', 'Face of Jesus ', 'Filigree', 'Freeform ', 'Gold Bar ', 'Graduated Squares ', 'Grandma ', 'Greek Cross ', 'Greek Key ', 'H Key ', 'Hammered ', 'Heavy Weight ', 'Holy Spirit', 'Infant of Prague ', 'Kings Crown ', 'Latin Cross ', 'Link ', 'Love You Always ', 'Matka Boska ', 'Matka Boska Medal ', 'Mezuzah ', 'Miraculous ', 'Mom ', 'Moon ', 'Moveable ', 'Music Note ', 'Number 1 ', 'Number Top ', 'Omega ', 'Orange ', 'Orthodox Cross ', 'Our Lady of Fatima ', 'Our Lady of Lourdes ', 'Our Lady of Mount Carmel ', 'Our Lady of Perpetual Help ', 'Oval ', 'Passion Cross ', 'Patonce Cross ', 'Patterned ', 'Photo ', 'Puffed ', 'Puffed Heart ', 'Radiant Essence ', 'Rectangle ', 'Rectangular ', 'Rectangular Shaped ', 'Recycle ', 'Roller Skate ', 'Rope ', 'Rope Cross ', 'Round ', 'Sacred Heart of Jesus ', 'Sacred Heart of Mary ', 'Scalloped Disc ', 'Scapular ', 'Scroll ', 'Shield ', 'Special Sister ', 'Square ', 'Square Swirl ', 'Square with Circles ', 'St. Elizabeth ', 'St. Gerard ', 'St. James ', 'St. Jude Thaddeus ', 'St. Martha ', 'St. Padre Pio ', 'St. Roch ', 'Strength ', 'Sun Cross ', 'Sunburst ', 'Swirl ', 'Swirl Cross ', 'Swirl Design ', 'Swirled Heart ', 'Tear Drop ', 'Teardrop ', 'Textured ', 'Top ', 'Triangular ', 'Twisted ', 'Twisted Rope ', 'US Coast Guard ', 'Wavy Circle ', 'Whale Tail ', 'flower']    ,
# 'charms'    :    ['Animals', 'Awareness Ribbons', 'Branded', 'Firefighter', 'Flowers', 'Trees', 'Plants', 'Food ', 'Drink', 'Fraternal', 'Hearts ', 'Horoscope', 'Irish', 'Celtic', 'Claddagh', 'Letters', 'Numbers', 'Names', 'Life Events', 'Locations ', 'Places', 'Medical', 'Military', 'Music', 'Nautical', 'Patriotic', 'People ', 'Family', 'Planes', 'Trains', 'Autos', 'Police', 'Religious', 'Shoes ', 'Clothing', 'Skulls', 'Sororities', 'Sports ', ' Games', 'Sports Teams', 'Sun', 'Moon', 'Stars', 'Talking', 'Tools', 'Trade', 'Wedding Gifts']    ,
# #'characteristics'    :    ['sparkle', 'shining', 'luminous', 'light', 'weight', 'heavy', 'versatile', 'sturdy', 'glint']    ,
# #'price'    :    ['affordable', 'cheap']    ,
# #'material'    :    ['.925 sterling silver', '10k gold', 'Swarovski crystal', 'acrylic', 'amber', 'amethyst', 'aquamarine', 'birthstone', 'black pearl', 'carnelian', 'ceramic', 'citrine', 'copper', 'mother of pearl', 'obsidia', 'onyx', 'pearl', 'peridot', 'platinum', 'precious gem', 'resin', 'rhinestone', 'rhodium', 'ruby', 'sapphire', 'seed beads', 'crystal', 'cubic zirconia', 'diamond', 'emerald', 'enamel', 'freshwater pearl', 'garnet', 'gem', 'gemstone', 'jade', 'laboratory gemstone', 'lapis', 'lucite', 'semi-precious stones', 'silver', 'simulated gemstone', 'stainless steel', 'titanium', 'topaz', 'tungsten', 'turquoise', 'white gold', 'wood', 'yellow gold', 'bead']    ,
# #'discount'    :    ['honest opinion', 'percent','%','off','deal','promo','promotion','sale','gift']    ,
# 'packing'    :    ['wrap', 'securely', 'enclosed', 'warranty', 'boxes']    ,
# #'quality'    :    ['abrasion', 'break', 'cheap', 'chipped', 'clash', 'digs in', 'poor fit', 'poor quality', 'rigid', 'rub', 'run-of-the-mill', 'scratch', 'scratches easily', 'falls apart', 'flawed', 'full-priced', 'heavy', 'ill-fitting', 'irregularities', 'irritation', 'tight', 'uncomfortable', 'unfashionable', 'unflattering', 'unwearable', 'wear and tear']    ,
# #'appearance'    :    ['loved it', 'love', 'stunning', 'beautiful', 'brilliant', 'shining', 'bling', 'highly recommend', 'pretty', 'elegant', 'classy', 'compliments', 'antique', 'endearing', 'perfect', 'pro']    ,
# #'style'    :    ['Rings', 'Earrings', 'Pendants', 'Charms', 'Bracelets', 'Necklaces', 'Chains', 'Collections', 'Anklets', 'Watches', 'Closeouts']    ,
# #'colour'    :    ['Yellow Gold', 'Sterling Silver ', 'Stainless Steel ', 'White Gold ', 'White And Yellow Gold ', 'Gold Filled ', 'Rose And White Gold ', 'Tungsten', 'Titanium', 'Rose Gold', 'Tri Color Gold', 'Rhodium Flashed Silver', 'Yellow Gold Flashed Silver', 'Rhodium Plated Silver']    ,
# 'gender and relationship'    :    ['male', 'female', 'girl', 'grandmother', 'daughter', 'kid', 'child', 'wife', 'husband', 'sister', 'daughter in law', 'mother in law', 'sister in law', 'girl friend', 'boyfriend', 'son', 'friend']    ,
# #'shipping'    :    ['fast', 'quick', 'eta', 'late', 'early', 'delivery', 'ahead', 'promt', 'undamaged']    ,
# #'feature'    :    ['adjustable', 'adorable', 'antique', 'artisan', 'artisanal', 'attention-getting', 'bangle-style', 'beaded', 'beautiful', 'bejeweled', 'bold', 'brilliant', 'burnished', 'carved', 'casual', 'certified', 'channel-set', 'charming', 'chic', 'chunky', 'classic', 'clustered', 'colorful', 'comfortable', 'comfy', 'complex', 'contemporary', 'cool', 'coordinating', 'corrosion-resistant', 'costume', 'crackled', 'cut-out', 'cute', 'cutting-edge', 'dainty', 'dangling', 'dangly', 'dapper', 'decorative', 'delicate', 'dependable', 'designer', 'detailed', 'discounted', 'distinctive', 'dramatic', 'durable', 'easy-to-maintain', 'eco-friendly', 'edgy', 'elegant', 'inlaid', 'innovative', 'intricate', 'iridescent', 'jewel-tone', 'keepsake', 'kiln-fired', 'large', 'laser-cut', 'latest', 'lead-free', 'lightweight', 'long-lasting', 'lovely', 'marbled', 'marquise-cut', 'masculine', 'men', 'metallic', 'minimalist', 'mosaic', 'multi-faceted', 'multistrand', 'nickel-free', 'one-of-a-kind', 'opaque', 'open-worked', 'original', 'ornate', 'period', 'pierced', 'plated', 'platinum', 'polished', 'pre-formed', 'precious', 'precise', 'premium-grade', 'preppy', 'princess-cut', 'prismatic', 'professional', 'radiant', 'reflective', 'reliable', 'rocker-style', 'rough-cut', 'round-cut', 'rugged', 'rust-resistant', 'sale-priced', 'sassy', 'encased', 'engineered', 'engraved', 'etched', 'everyday', 'exceptional', 'exciting', 'exotic', 'expensive-looking', 'exquisite', 'eye-catching', 'faceted', 'fancy', 'fashion-forward', 'fashionable', 'favorite', 'feminine', 'fine', 'finely detailed', 'finished with', 'flattering', 'flawless', 'flexible', 'flirty', 'floral', 'funky', 'genuine', 'glamorous', 'glittering', 'glitzy', 'gold', 'gold-filled', 'gold-plated', 'gold-toned', 'gorgeous', 'graceful', 'gunmetal', 'half-priced', 'hammered', 'hand-carved', 'hand-crafted', 'hand-finished', 'hand-hammered', 'hand-wrapped', 'handcrafted', 'handmade', 'hassle-free', 'high-class', 'high-performance', 'hinged', 'hot', 'hypoallergenic', 'scratch-resistant', 'semi-translucent', 'set', 'sexy', 'shimmering', 'silver', 'silver-tone', 'simple', 'sleek', 'slender', 'slip-on', 'small', 'smoky', 'smooth', 'snag-free', 'solid', 'sophisticated', 'sparkling', 'sparkly', 'sporty', 'streamlined', 'striking', 'structural', 'studded', 'stunning', 'stylish', 'subtle', 'superior', 'supportive', 'suspended', 'tapered', 'teardrop', 'textured', 'timeless', 'tiny', 'top-of-the-line', 'trendsetting', 'tribal', 'two-tone', 'unique', 'versatile', 'vintage', 'wardrobe-friendly', 'water-resistant', 'waterproof', 'wear-anywhere', 'whimsical', 'women', 'wooden', 'wrapped'],
# #'packing bad'    :    ['worn', 'broken', 'lost', 'bent', 'spin', 'fall out', 'out', 'loose', 'discolouration', 'damaged'],
# #'service':['service','delivery','greet','consumer','help','care']}


# In[3]:


'''
type_tag
'''
tag_dict = {'characteristics' : ['Yellow Gold', 'Sterling Silver ', 'Stainless Steel ', 'White Gold ', 'White And Yellow Gold ', 'Gold Filled ', 'Rose And White Gold ', 'Tungsten', 'Titanium', 'Rose Gold', 'Tri Color Gold', 'Rhodium Flashed Silver', 'Yellow Gold Flashed Silver', 'Rhodium Plated Silver','loved it', 'love', 'stunning', 'beautiful', 'brilliant', 'shining', 'bling', 'highly recommend', 'pretty', 'elegant', 'classy', 'compliments', 'antique', 'endearing', 'perfect', 'pro','sparkle', 'shining', 'luminous', 'light', 'weight', 'heavy', 'versatile', 'sturdy', 'glint','3 Dimensional', 'Abstract ', 'Angel ', 'Antiqued ', 'Ball Chain ', 'Baptism', 'Baroque Cross ', 'Bead', 'Brushed ', 'Budded Cross', 'Bullet ', 'Cat ', 'Chai ', 'Chandelier ', 'Charm Holder', 'Circle', 'Circular', 'Coin Holder ', 'Concave ', 'Confirmation ', 'Cord ', 'Crown ', 'Crown Heart ', 'Cylinder', 'Dangle ', 'Daughter ', 'Desire Heart ', 'Diamond Shape ', 'Dove ', 'Drop ', 'Ecce Homo', 'Eternal Life ', 'Eye ', 'Face of Jesus ', 'Filigree', 'Freeform ', 'Gold Bar ', 'Graduated Squares ', 'Grandma ', 'Greek Cross ', 'Greek Key ', 'H Key ', 'Hammered ', 'Heavy Weight ', 'Holy Spirit', 'Infant of Prague ', 'Kings Crown ', 'Latin Cross ', 'Link ', 'Love You Always ', 'Matka Boska ', 'Matka Boska Medal ', 'Mezuzah ', 'Miraculous ', 'Mom ', 'Moon ', 'Moveable ', 'Music Note ', 'Number 1 ', 'Number Top ', 'Omega ', 'Orange ', 'Orthodox Cross ', 'Our Lady of Fatima ', 'Our Lady of Lourdes ', 'Our Lady of Mount Carmel ', 'Our Lady of Perpetual Help ', 'Oval ', 'Passion Cross ', 'Patonce Cross ', 'Patterned ', 'Photo ', 'Puffed ', 'Puffed Heart ', 'Radiant Essence ', 'Rectangle ', 'Rectangular ', 'Rectangular Shaped ', 'Recycle ', 'Roller Skate ', 'Rope ', 'Rope Cross ', 'Round ', 'Sacred Heart of Jesus ', 'Sacred Heart of Mary ', 'Scalloped Disc ', 'Scapular ', 'Scroll ', 'Shield ', 'Special Sister ', 'Square ', 'Square Swirl ', 'Square with Circles ', 'St. Elizabeth ', 'St. Gerard ', 'St. James ', 'St. Jude Thaddeus ', 'St. Martha ', 'St. Padre Pio ', 'St. Roch ', 'Strength ', 'Sun Cross ', 'Sunburst ', 'Swirl ', 'Swirl Cross ', 'Swirl Design ', 'Swirled Heart ', 'Tear Drop ', 'Teardrop ', 'Textured ', 'Top ', 'Triangular ', 'Twisted ', 'Twisted Rope ', 'US Coast Guard ', 'Wavy Circle ', 'Whale Tail ', 'flower','Rings', 'Earrings', 'Pendants', 'Charms', 'Bracelets', 'Necklaces', 'Chains', 'Collections', 'Anklets', 'Watches', 'Closeouts''adjustable', 'adorable', 'antique', 'artisan', 'artisanal', 'attention-getting', 'bangle-style', 'beaded', 'beautiful', 'bejeweled', 'bold', 'brilliant', 'burnished', 'carved', 'casual', 'certified', 'channel-set', 'charming', 'chic', 'chunky', 'classic', 'clustered', 'colorful', 'comfortable', 'comfy', 'complex', 'contemporary', 'cool', 'coordinating', 'corrosion-resistant', 'costume', 'crackled', 'cut-out', 'cute', 'cutting-edge', 'dainty', 'dangling', 'dangly', 'dapper', 'decorative', 'delicate', 'dependable', 'designer', 'detailed', 'discounted', 'distinctive', 'dramatic', 'durable', 'easy-to-maintain', 'eco-friendly', 'edgy', 'elegant', 'inlaid', 'innovative', 'intricate', 'iridescent', 'jewel-tone', 'keepsake', 'kiln-fired', 'large', 'laser-cut', 'latest', 'lead-free', 'lightweight', 'long-lasting', 'lovely', 'marbled', 'marquise-cut', 'masculine', 'men', 'metallic', 'minimalist', 'mosaic', 'multi-faceted', 'multistrand', 'nickel-free', 'one-of-a-kind', 'opaque', 'open-worked', 'original', 'ornate', 'period', 'pierced', 'plated', 'platinum', 'polished', 'pre-formed', 'precious', 'precise', 'premium-grade', 'preppy', 'princess-cut', 'prismatic', 'professional', 'radiant', 'reflective', 'reliable', 'rocker-style', 'rough-cut', 'round-cut', 'rugged', 'rust-resistant', 'sale-priced', 'sassy', 'encased', 'engineered', 'engraved', 'etched', 'everyday', 'exceptional', 'exciting', 'exotic', 'expensive-looking', 'exquisite', 'eye-catching', 'faceted', 'fancy', 'fashion-forward', 'fashionable', 'favorite', 'feminine', 'fine', 'finely detailed', 'finished with', 'flattering', 'flawless', 'flexible', 'flirty', 'floral', 'funky', 'genuine', 'glamorous', 'glittering', 'glitzy', 'gold', 'gold-filled', 'gold-plated', 'gold-toned', 'gorgeous', 'graceful', 'gunmetal', 'half-priced', 'hammered', 'hand-carved', 'hand-crafted', 'hand-finished', 'hand-hammered', 'hand-wrapped', 'handcrafted', 'handmade', 'hassle-free', 'high-class', 'high-performance', 'hinged', 'hot', 'hypoallergenic', 'scratch-resistant', 'semi-translucent', 'set', 'sexy', 'shimmering', 'silver', 'silver-tone', 'simple', 'sleek', 'slender', 'slip-on', 'small', 'smoky', 'smooth', 'snag-free', 'solid', 'sophisticated', 'sparkling', 'sparkly', 'sporty', 'streamlined', 'striking', 'structural', 'studded', 'stunning', 'stylish', 'subtle', 'superior', 'supportive', 'suspended', 'tapered', 'teardrop', 'textured', 'timeless', 'tiny', 'top-of-the-line', 'trendsetting', 'tribal', 'two-tone', 'unique', 'versatile', 'vintage', 'wardrobe-friendly', 'water-resistant', 'waterproof', 'wear-anywhere', 'whimsical', 'women', 'wooden', 'wrapped'],  
'quality' : ['.925 sterling silver', '10k gold', 'Swarovski crystal', 'acrylic', 'amber', 'amethyst', 'aquamarine', 'birthstone', 'black pearl', 'carnelian', 'ceramic', 'citrine', 'copper', 'mother of pearl', 'obsidia', 'onyx', 'pearl', 'peridot', 'platinum', 'precious gem', 'resin', 'rhinestone', 'rhodium', 'ruby', 'sapphire', 'seed beads', 'crystal', 'cubic zirconia', 'diamond', 'emerald', 'enamel', 'freshwater pearl', 'garnet', 'gem', 'gemstone', 'jade', 'laboratory gemstone', 'lapis', 'lucite', 'semi-precious stones', 'silver', 'simulated gemstone', 'stainless steel', 'titanium', 'topaz', 'tungsten', 'turquoise', 'white gold', 'wood', 'yellow gold', 'bead','abrasion', 'break', 'cheap', 'chipped', 'clash', 'digs in', 'poor fit', 'poor quality', 'rigid', 'rub', 'run-of-the-mill', 'scratch', 'scratches easily', 'falls apart', 'flawed', 'full-priced', 'heavy', 'ill-fitting', 'irregularities', 'irritation', 'tight', 'uncomfortable', 'unfashionable', 'unflattering', 'unwearable', 'wear and tear'],
'price' : ['affordable', 'cheap','honest opinion', 'percent','%','off','deal','promo','promotion','sale','gift'],
'customer service' : ['wrap', 'securely', 'enclosed', 'warranty', 'boxes','worn', 'broken', 'lost', 'bent', 'spin', 'fall out', 'out', 'loose', 'discolouration', 'damaged','service','delivery','greet','consumer','help','care','fast', 'quick', 'eta', 'late', 'early', 'delivery', 'ahead', 'promt', 'undamaged']           
}


# In[3]:


'''
product tags
'''
# tag_dict ={'charms':['charms-clip','charms','beads','beads and bead assortments'],
#                'bracelet': ['wrist watches','bangle','bracelet','bracelets','strand',''],
#                'earrings' : ['earrings','stud','hoops'],
#                'Necklaces and pendants':['chains','necklaces','pendants','clasp'],
#                'others' : ['beading supplies','beading and jewelry making','jewelry findings','jewelry','body jewelrty','jewelry boxes and organizers'],
#                'rings' : ['rings','snake']
    
}


# In[4]:


# path = "C:\Users\user\Documents\social_media_analytics\pandor-voc\data\tags_final"
data_reviews = pd.read_csv('cleaned_dt_product_tag_for_all.csv', sep=",", encoding='utf-8')
data_reviews.drop(["Unnamed: 0"], axis=1, inplace=True)
data_reviews.head()


# In[13]:


am_review = data_reviews.loc[data_reviews['source'] == "PissedConsumer"]
am_review['index']= range(0, len(am_review)) # this is the line that generates the warning
am_review = am_review.set_index('index')

#am_review = am_review.set_index
am_review.head()


# In[5]:


import string

is_punct = string.punctuation
nlp = en_core_web_md.load()
def get_tags(review):
    threshold = .8
    # score = has_brand_comparison(review)
    score = []
    try:
        review = nlp(dedupe_str(review.decode('utf-8')))
    except Exception:
        print review
    for token in review:
        if not (token.text in is_punct or token.is_stop):
            score += [
                tag_key
                for tag_key, tags in tag_dict.iteritems()
                if tag_key not in score and get_max_score(token, tags) > threshold
            ]
        if len(score) == len(tag_dict):
            break
    return score


def dedupe_str(string_input):
    return ' '.join(set(re.split(r'[^\w$]', string_input)))


def get_max_score(token, tags):
    return max(map(lambda x: nlp(unicode(x.lower())).similarity(token), tags))


# In[ ]:


'''
returns tags 
'''
from tqdm import tqdm
test = time.time()
tags = []
for i in tqdm(range(len(data_reviews))):
    try:
        tags.append(get_tags(data_reviews["cleaned_review"][i]))
        print(get_tags(data_reviews["cleaned_review"][i]))
    except:
        tags.append(["none"])
print(test-time.time())


# In[ ]:


len(tags)


# In[18]:


data_reviews['type_tags'] = tags


# In[19]:


data_reviews.head()


# In[15]:


data_reviews.to_csv("cleaned_dt_product_tag_for_all.csv", sep=",", encoding ='utf-8' )


# In[79]:


def week_end_dt_find(given_date):
    given_date = datetime.strptime(given_dae,'%d-%m-%Y')
    if(given_date.weekday())>5:
        x= -1
    else:
        x = 5-(given_date.weekday())
    return(given_date+timedelta(days=x))
    
    
    
#am_review['Week_end_dt'] = am_review['Date'].apply(week_end_dt_find)
print am_review.columns
#am_agg = am_review.groupby(['Sub_Category','Week_end_dt','Sentiment'])['Score'].agg({'num_comment':'count'}).reset_index()
#am_agg.to_csv("Amazon_with_scores.csv", index = False)

# twitter
tw = pd.read_csv("Jewelry_twt.csv")

#tw['Week_end_dt']= tw['Date'].apply(week_end_dt_find)
tw_agg = tw.groupby(['Sentiment'])['Score'].agg({'num_comment':'count'}).reset_index()
#tw_agg.to_csv('tw_agg.csv', index = False)

# Facebook

fb = pd.read_csv("Fb_comments.csv")
fb['Week_end_dt'] = fb['Date'].apply(week_end_dt_find)
fb_agg = fb.groupby(['Week_end_dt','Sentiment'])['Score'].agg({'num_comment':'count'}).reset_index()
#fb_agg.to_csv('fb_agg.csv', index = False)


# In[2]:


final_df = pd.DataFrame(tags)
final_df.to_csv("twitter_tagging.csv", sep=",")


# In[17]:


tags =[['material', 'style', 'adjectives', 'theme'], ['theme'], ['material', 'style', 'adjectives', 'theme', 'positives', 'packing bad'], ['theme'], ['theme'], ['style'], ['positives'], ['style'], ['material', 'style', 'adjectives', 'theme'], ['material', 'style', 'adjectives', 'theme', 'positives'], [], [], [], [], [], [], [], ['theme'], ['theme'], ['positives', 'theme', 'material', 'style', 'adjectives'], ['material', 'style', 'adjectives', 'theme'], [], [], ['positives', 'theme'], [], [], [], ['positives', 'theme', 'charms', 'gender and relationship'], ['gender and relationship', 'theme'], [], ['positives', 'theme', 'adjectives', 'gender and relationship'], ['theme'], ['positives', 'theme'], ['gender and relationship', 'theme'], ['theme'], ['theme', 'positives'], ['charms', 'gender and relationship', 'theme'], ['theme', 'charms', 'positives'], [], ['gender and relationship', 'theme'], ['gender and relationship', 'theme'], ['gender and relationship', 'theme']] 


# In[18]:


type(tags)


# In[57]:


import csv

with open("output_amazon_month_1.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(tags)


# In[20]:


len(tags)


# In[33]:


data_consumer_product = pd.read_csv("/home/apeksha/Downloads/amazon_tagged_product_new.csv")


# In[34]:


data_consumer_type = pd.read_csv("/home/apeksha/Downloads/amazon_tagged_types_new.csv")


# In[35]:


data_consumer_type.head()


# In[36]:


data_consumer_product['type_tag'] = data_consumer_type['type_tags']


# In[37]:


data_consumer_product.head()


# In[38]:


data_consumer_product.to_csv("output_final_amazon.csv")

