from bs4 import BeautifulSoup as bs
import requests as rs
import pandas as pd

'''basic link'''
linkfirst="http://www.morapandorablog.com"
comments = []

'''loop through pages, 15 because ik there r 15 pages'''
for i in range(15):
    '''
    if else if because page 1 and rest have different second part
    of url
    '''
    if i==0:
        linked1="/category/review/"
        link=linkfirst+linked1
        html= rs.get(link)
        soup = bs(html.text, 'html.parser')
        sections2 = soup.find_all("a", class_="post-read-more")
        next_page_urls = soup.find_all("a",class_="next page-numbers", href = True)
        next_page_url = next_page[0].get("href")
        '''loop through sections in the page, each page has 10 sections'''
        for t in range(len(sections2)):
            article_link=sections2[t].get("href")

            each_article = rs.get(article_link)
            soup_comment1 = bs(each_article.text, 'html.parser')

            next_page2 = (soup_comment1.find_all("div", class_="comment-content"))
            '''
            loop through all comments and replies to the comments
            '''
            for comment in next_page2:
                comments.append(comment.text)

    else:
        link=linkfirst+next_page_url
        html= rs.get(link)
        soup = bs(html.text, 'html.parser')
        sections2 = soup.find_all("a", class_="post-read-more")
        next_page_urls = soup.find_all("a",class_="next page-numbers", href = True)
        next_page_url = next_page[0].get("href")
        for t in range(len(sections2)):
            article_link=sections2[t].get("href")

            each_article = rs.get(article_link)
            soup_comment1 = bs(each_article.text, 'html.parser')

            next_page2 = (soup_comment1.find_all("div", class_="comment-content"))
            for comment in next_page2:
                comments.append(comment.text)

'''
saving to a dataframe and then to csv file
'''
df = pd.DataFrame(comments, columns=['morapandorablog_comments'])
df.to_csv("morapandorablog_comments",sep=",",encoding="utf-8")
